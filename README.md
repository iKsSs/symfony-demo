# Symfony REST - DEMO
____

### Setup

#### Start the project
`docker-compose up --build`

#### Install composer packages
`docker-compose exec php composer install`

#### Apply migrations
`docker-compose exec php bin/console doctrine:migrations:migrate`

#### *Optional* - Apply fixtures (load testing data into DB)
`docker-compose exec php bin/console doctrine:fixtures:load --append`

### Connect to the database from your IDE

- **Host:** localhost
- **Port:** 3306
- **Database:** symfony
- **User:** admin
- **Password:** f1Jc5rJ61g

<br>

### Server address
`http://localhost:54321`

<br>

#### REST
##### Registration
POST `/api/register`
- `{
  "email": "<EMAIL>",
  "password": "<PASSWORD>",
  "name": "<NAME>"
  }`

##### Authentication
POST `/api/authenticate`
- `{
  "email": "<EMAIL>",
  "password": "<PASSWORD>"
  }`

##### Get User info (*after successful authentication*)

GET `/api/user/get` +  Header `Authorization: Bearer <TOKEN>`

<br>

#### SOAP
`http://localhost:54321/soap/register?wsdl`

<br>

### Useful commands

#### Shell into PHP container
`docker-compose exec php bash`

#### Create DB tables/schema
`docker-compose exec php bin/console make:migration`

#### Clear symfony cache
`docker-compose exec php bin/console cache:clear`

#### Execute SQL query
`docker-compose exec php bin/console doctrine:query:sql '<SQL>'`

#### Fully recreate containers
`docker-compose down --volumes && docker-compose up --build`

#### Fully recreate containers (clear database)
`docker-compose down --volumes && rm -rf docker/db/data && docker-compose up --build`

#### Fully recreate containers (clear composer packages)
`docker-compose down --volumes && rm -rf var vendor && docker-compose up --build`
