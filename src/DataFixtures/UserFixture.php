<?php

namespace App\DataFixtures;

use App\Entity\ApiToken;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends BaseFixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

//    public function load(ObjectManager $manager)
//    {
//        $user = new User();
//        $user->setEmail('test@email.cz');
//
//        $user->setPassword($this->passwordEncoder->encodePassword(
//            $user,
//            'password'
//        ));
//
//        $manager->persist($user); 
//        $manager->flush();
//    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(5, 'main_users', function($i) use ($manager) {
            $user = new User();
            $user->setEmail(sprintf('test%d@email.cz', $i));
            $user->setName(sprintf('name%d', $i * random_int(1, 15)));
            $user->setRoles(['ROLE_USER']);

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'password'
            ));

            return $user;
        });

        $this->createMany(3, 'admin_users', function($i) {
            $user = new User();
            $user->setEmail(sprintf('admin%d@email.cz', $i));
            $user->setName(sprintf('admin%d', $i * random_int(1, 19)));
            $user->setRoles(['ROLE_ADMIN']);

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'password'
            ));

            return $user;
        });

        $manager->flush();
    }
}
