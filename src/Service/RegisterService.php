<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterService
{
    private $passwordEncoder;
    private $em;
    private $validator;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
        $this->validator = $validator;
    }

    public function register(string $email, string $password, string $name): string
    {
        if (!strlen($email) || !strlen($password))
        {
            return 'Email and password have to be set';
        }

        $db_user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if ($db_user)
        {
            return 'User with this email is already registered.';
        }

        $user = new User();
        $user->setEmail($email);
        $user->setRoles(["ROLE_USER"]);
        $user->setName($name);

        // encode the plain password
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $password
            )
        );

        $this->em->persist($user);
        $this->em->flush();

        return 'Registration was successful';
    }
}
