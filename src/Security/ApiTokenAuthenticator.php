<?php

namespace App\Security;

use App\Entity\ApiToken;
use App\Repository\ApiTokenRepository;
use App\Repository\UserRepository;
use App\Service\Json;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    private $apiTokenRepo;
    private $userRepo;
    private $passwordEncoder;
    private $json;
    private $em;

    static private $data;

    private const REGISTRATION_ROUTE = 'api_register';

    public function __construct(ApiTokenRepository $apiTokenRepo, UserRepository $userRepo, UserPasswordEncoderInterface $passwordEncoder, Json $json, EntityManagerInterface $em)
    {
        $this->apiTokenRepo = $apiTokenRepo;
        $this->userRepo = $userRepo;
        $this->passwordEncoder = $passwordEncoder;
        $this->json = $json;
        $this->em = $em;
    }

    public function supports(Request $request): ?bool
    {
        self::$data = $this->json->getJson($request);

        // look for header "Authorization: Bearer <token>"
        return ($request->headers->has('Authorization')
            && str_starts_with($request->headers->get('Authorization'), 'Bearer '))
            ||
            (!self::$data instanceof JsonResponse && self::$data['email'] && self::$data['password']
            &&
            self::REGISTRATION_ROUTE !== $request->attributes->get('_route'));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // allow the authentication to continue
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return new JsonResponse([
            'message' => $exception->getMessageKey()
        ], 401);
    }

    public function getCredentials(Request $request): array
    {
        $authorizationHeader = $request->headers->get('Authorization');

        $credentials = [
            'email' => self::$data instanceof JsonResponse ? null : self::$data['email'],
            'password' => self::$data instanceof JsonResponse ? null : self::$data['password'],
            'token' => substr($authorizationHeader, 7), // skip beyond "Bearer "
        ];

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider): \App\Entity\User
    {
        $token = $this->apiTokenRepo->findOneBy([
            'token' => $credentials['token']
        ]);

        if (!$token)
        {
            if (!$credentials['email'])
            {
                throw new CustomUserMessageAuthenticationException(
                    'You have to authenticate'
                );
            }

            $user = $this->userRepo->findOneBy([
                'email' => $credentials['email']
            ]);

            if (!$user)
            {
                throw new CustomUserMessageAuthenticationException(
                    'User not found'
                );
            }
            elseif (!$this->passwordEncoder->isPasswordValid($user, $credentials['password']))
            {
                throw new CustomUserMessageAuthenticationException(
                    'Wrong credentials'
                );
            }

            $token = new ApiToken($user);
            $this->em->persist($token);
            $this->em->flush();
        }
        else
        {
            if ($token->isExpired())
            {
                throw new CustomUserMessageAuthenticationException(
                    'Token expired'
                );
            }
            else
            {
                $token->renewExpiresAt();
            }
        }

        return $token->getUser();
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new \Exception('Not used: entry_point from other authentication is used');
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }
}
