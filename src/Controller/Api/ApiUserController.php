<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\Json;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class ApiUserController extends AbstractController
{
    /**
     * @Route("/user/get", name="api_user_get", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function userGet(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->json($user, 200, [], [
            'groups' => ['main'],
        ]);
    }

    /**
     * @Route("/authenticate", name="api_authenticate", methods={"POST"})
     */
    public function authenticate(Request $request, Json $json, UserRepository $userRepo): Response
    {
        $data = $json->getJson($request);

        $email = $data['email'];

        $user = $userRepo->findOneBy(['email' => $email]);

        if ($user)
        {
            return new JsonResponse(["token" => $user->getApiTokens()->first()->getToken()], 200);
        }

        return new JsonResponse(["error" => "Wrong authentiacation"], 500);
    }

    /**
     * @Route("/register", name="api_register", methods={"POST"})
     */
    public function register(Request $request, Json $json, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $data = $json->getJson($request);

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'email' => new Assert\Email(),
            'password' => new Assert\Length(array('min' => 1)),
            'name' => new Assert\Length(array('min' => 1)),
        ));

        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string)$violations], 500);
        }

        $email = $data['email'];
        $password = $data['password'];
        $name = $data['name'];

        $db_user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        if ($db_user)
        {
            return new JsonResponse(['error' => 'User with this email is already registered.']);
        }

        $user = new User();

        $hashed_password = $passwordEncoder->encodePassword($user, $password);

        $user->setPassword($hashed_password);
        $user->setEmail($email);
        $user->setName($name);
        $user->setRoles(["ROLE_USER"]);

        try
        {
            $em->persist($user);
            $em->flush();
        }
        catch (\Exception $e)
        {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }

        return new JsonResponse(["success" => $user->getUsername(). " has been registered!"], 200);
    }
}
