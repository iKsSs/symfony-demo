<?php

namespace App\Controller;

use App\Service\RegisterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegisterServiceController extends AbstractController
{
    /**
     * @Route("/soap/register", name="soap_register")
     */
    public function register(RegisterService $registerService): Response
    {
        $soapServer = new \SoapServer('wsdl/register.wsdl');
        $soapServer->setObject($registerService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $soapServer->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}